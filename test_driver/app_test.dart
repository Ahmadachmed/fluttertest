import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Gallery App : ', () {
    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      await driver.close();
    });

    test('Given user go to the Shrine Gallery', () async {
      final card = find.byValueKey('studyDemoList');
      final shrine = find.text('Shrine');
      final userName = find.byValueKey('username');
      final password = find.byValueKey('password');
      final next = find.text('NEXT');

      await driver.scrollUntilVisible(card, shrine, dxScroll: 300.0);
      await driver.tap(shrine);
      await driver.tap(userName);
      await driver.enterText('test');
      await driver.tap(password);
      await driver.enterText('testpassword');
      await driver.tap(next);
    });

    test('When user add Walter Henley (white) shirt to the chart', () async {
      final menu = find.byValueKey('menu');
      final clothing = find.text('CLOTHING');
      final productList = find.byValueKey('productList');
      final walterHenley = find.text('Walter henley (white)');
      final cart = find.byValueKey('cart');
      final item = find.byValueKey('item');

      await driver.tap(menu);
      await driver.tap(clothing);
      await driver.scrollUntilVisible(productList, walterHenley, dxScroll: -300.0);
      await driver.tap(walterHenley);
      await driver.tap(cart);
      expect(await driver.getText(item), '1 ITEM');
    });

    test('And user add the Shrug bag after using the Accessories filter', () async {
      final dropDownBtn = find.byValueKey('dropDownBtn');
      final menu = find.byValueKey('menu');
      final accessories = find.text('ACCESSORIES');
      final shrugBag = find.text('Shrug bag');
      final productList = find.byValueKey('productList');
      final cart = find.byValueKey('cart');
      
      await driver.tap(dropDownBtn);
      await driver.tap(menu);
      await driver.tap(accessories);
      await driver.scrollUntilVisible(productList, shrugBag, dxScroll: -600.0);
      await driver.tap(shrugBag);
      await driver.tap(cart);

    });

    test('Then user check the total of the shoping cart and clear the cart', () async {
      final item = find.byValueKey('item');
      final clearCart = find.text('CLEAR CART');

      expect(await driver.getText(item), '2 ITEMS');
      await driver.tap(clearCart);
    });


  });
}