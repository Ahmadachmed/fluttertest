## Running Flutter Gallery

The Flutter Gallery targets Flutter's master channel. As such, it can take advantage
of new SDK features that haven't landed in the stable channel.

If you'd like to run the Flutter Gallery, make sure to switch to the master channel
first:

```bash
flutter channel master
flutter upgrade
```

When you're done, use this command to return to the safety of the stable
channel:

```bash
flutter channel stable
flutter upgrade
```

## Integration Test

1. The instrumented version of the app you can find at /test_driver/app.dart
2. The test scenario you can find at /test_driver/app_test.dart
3. Run the test using the following command from the root of the project
```bash
flutter drive --target=test_driver/app.dart
```

## To fit automated test in the SDLC

1. Add to the pipeline request for unit testing, widget testing and integration testing
2. Don't forget to add the deployment trigger for CI.
3. Do monitoring for the test result
